export default function normalise(value, type){
	switch(type){
		case 'tel':

			// first let's replace signs and spaces;
			let val = value.replace(/-|\(|\)|\s/g, '');

			// then let's normalise country code (remove it in this case)
			if (val.indexOf('00') == 0) val = val.replace('00','');
			if (val.indexOf('0') == 0) val = val.replace('0','');
			if (val.indexOf('+48') == 0) val = val.replace('+48','');

			return val;
		default:
			return value;
	}
}