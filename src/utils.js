/**
 * A shorthand function to get elements whether a single dom element is passed or a string is passed
 * @param  {string or DOMElement} el
 * @param  {DOMElement under which we will look for elements} context
 * @return {DOMElement} A single element returned by our query
 */
export function getEl(el, context = document){

    if (typeof el === 'string'){
        if(el.indexOf('#') == 0 && el.indexOf(' ') <= 0 ){
            return document.getElementById(el.substr(1));
        }else{
            return context.querySelector(el);
        }
    } else if (el instanceof HTMLElement){
        return el;
    } else {
        console.log('passed el is not HTML Element');
        return;
    }
}

/**
 * A shorthand function to get elements whether a single dom element is passed or a string is passed
 * @param  {string} el
 * @param  {DOMElement under which we will look for elements} context
 * @return {DOMElement} Iterable elements returned by our query
 */
export function getEls(el, context = document){

    if (typeof el === 'string'){
        if( el.indexOf('.') == 0 && el.indexOf(' ') <= 0 ){
            return context.getElementsByClassName(el.substr(1));
        }else{
            return context.querySelectorAll(el);
        }
    }
}

/*
 * Get Viewport Dimensions
 * returns object with viewport dimensions to match css in width and height properties
 * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
 */
export function getViewportDimensions() {
    var w = window,
        d = document,
        e = d.documentElement,
        g = d.getElementsByTagName('body')[0],
        x = g.clientWidth,
        y = e.clientHeight ;
    return {
        width: x,
        height: y
    }
}


/**
 * event throttling
 * @param  {Object} ) { var timers [description]
 * @return {void}
 */
export function debounce (func, wait, immediate) {
     var timeout;
     return function() {
         var context = this, args = arguments;
         var later = function() {
                 timeout = null;
                 if (!immediate) func.apply(context, args);
         };
         var callNow = immediate && !timeout;
         clearTimeout(timeout);
         timeout = setTimeout(later, wait);
         if (callNow) func.apply(context, args);
     };
};

/**
 * Merge defaults with user options
 * @param {Object} defaults Default settings
 * @param {Object} options User options
 * @returns {Object} Merged values of defaults and options
 */
export function extend( defaults, options ) {
    // ES6
    if (typeof Object.assign === 'function'){
        return Object.assign({},defaults,options);
    // ES5
    }else{
        var extended = {};
        var prop;
        for (prop in defaults) {
            if (Object.prototype.hasOwnProperty.call(defaults, prop)) {
                extended[prop] = defaults[prop];
            }
        }
        for (prop in options) {
            if (Object.prototype.hasOwnProperty.call(options, prop)) {
                extended[prop] = options[prop];
            }
        }
        return extended;
    }
};
/**
 * Simple object check.
 * @param item
 * @returns {boolean}
 */
export function isObject(item) {
  return (item && typeof item === 'object' && !Array.isArray(item));
}

export function deepObjectExtend(target, source) {
  let output = Object.assign({}, target);
  if (isObject(target) && isObject(source)) {
    Object.keys(source).forEach(key => {
      if (isObject(source[key])) {
        if (!(key in target))
          Object.assign(output, { [key]: source[key] });
        else
          output[key] = deepObjectExtend(target[key], source[key]);
      } else {
        Object.assign(output, { [key]: source[key] });
      }
    });
  }
  return output;
}

/**
 * implements foreach for given iterable
 * @param  {iterable} arr      any iterable object
 * @return {undefined}
 */
export function forEach(arr, funct){
    Array.prototype.forEach.call(arr, function(a,b){
        funct(a,b);
    });
}

/**
 * implements foreach for given iterable object/array
 * @param  {iterable} arr any iterable object
 * @return {[type]}            [description]
 */
export function filter( arr, funct ) {

	return Array.prototype.filter.call(arr, function( a, b ) {

		return funct( a, b );

	} );

}

/**
 * implements indexOf for given iterable object/array
 * @param  {iterable} arr any iterable object
 * @return {[type]}            [description]
 */
export function indexOf(arr) {
    Array.prototype.indexOf.call(arr);
}

/**
 * inserts element after specified element
 * @param  {Node} newNode         the node we want added after referenceNode
 * @param  {Node} referenceNode   the node after which we add newNode
 * @return {Node}                 the new node (in different context?)
 */
export function insertAfter(newNode, referenceNode) {
   return referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}



export function render(string){

    const wrap = document.createElement('div');
    wrap.innerHTML = string;
    return wrap.children[0];

}

export function random(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * Travers up the node's parents and get first element
 * that matches the selector query
 * @param  {string} selector     CSS type selector
 * @param  {DOMElement} el       A base Element
 * @return {DOMElement}          Parent Element
 */
export function getParent( selector, el ) {

	while ( ( el = el.parentElement ) && !( ( el.matches || el.matchesSelector ).call( el, selector ) ) );
	return el;

}

/*! getClosest.js | (c) 2017 Chris Ferdinandi | MIT License | http://github.com/cferdinandi/getClosest */
/**
 * Get the closest parent element that matches a selector.
 * @param  {Element} elem     Starting element
 * @param  {String}  selector Selector to match against
 * @return {Boolean|Element}  Returns null if not match found
 */
export function getClosest(elem, selector) {
   // Element.matches() polyfill
    if (!Element.prototype.matches) {
        Element.prototype.matches =
            Element.prototype.matchesSelector ||
            Element.prototype.mozMatchesSelector ||
            Element.prototype.msMatchesSelector ||
            Element.prototype.oMatchesSelector ||
            Element.prototype.webkitMatchesSelector ||
            function(s) {
                var matches = (this.document || this.ownerDocument).querySelectorAll(s),
                    i = matches.length;
                while (--i >= 0 && matches.item(i) !== this) {}
                return i > -1;
            };
    }

    // Get closest match
    for ( ; elem && elem !== document; elem = elem.parentNode ) {
        if ( elem.matches( selector ) ) return elem;
    }

    return null;
}
