
export function loadFile( file ) {

	const fr = new FileReader();

	return new Promise( resolve => {

		fr.readAsDataURL( file );

		fr.addEventListener( 'load', ( e ) => {

			const rawImage = e.target.result;
			const image = new Image();
			image.src = rawImage;
			resolve( image );

		} );

	} );

}

/**
 * @param  {dataUrl} dataUrl
 * @return {Blob} a Blob containing the data to Save
 */
export function dataUrlToBlob(dataUrl){
    
    var arr = dataUrl.split(','), 
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), 
        n = bstr.length, 
        u8arr = new Uint8Array(n);
    
    while(n--){
            u8arr[n] = bstr.charCodeAt(n);
    }

    return new Blob([u8arr], {type:mime});

}