var browser = require('./src/browser');
var emitter = require('./src/emitter');
var eventQueue = require('./src/eventQueue');
var file = require('./src/file');
var loadIcons = require('./src/loadIcons');
var normalise = require('./src/normalise');
var onResize = require('./src/onResize');
var utils = require('./src/utils');

module.exports = {
    browser: browser.browser,

    Emitter: emitter.Emitter,

    eventQueue,

    loadFile: file.loadFile,
    dataUrlToBlob: file.dataUrlToBlob,

    loadIcons: loadIcons.loadIcons,

    normalise,

    onResize,

    getEl: utils.getEl,
    getEls: utils.getEls,
    getViewportDimensions: utils.getViewportDimensions,
    debounce: utils.debounce,
    extend: utils.extend,
    isObject: utils.isObject,
    deepObjectExtend: utils.deepObjectExtend,
    forEach: utils.forEach,
    filter: utils.filter,
    indexOf: utils.indexOf,
    insertAfter: utils.insertAfter,
    render: utils.render,
    random: utils.random,
    getParent: utils.getParent,
    getClosest: utils.getClosest,
};
